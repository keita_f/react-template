const gulp = require('gulp');
const browserify = require('browserify');
const source = require("vinyl-source-stream");
const reactify = require('reactify');

gulp.task('browserify', function(){
  const b = browserify({
    entries: ['./src/scripts/index.js'],
    transform: [reactify]
  });
  return b.bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest('./deploy'));
});