const React = require('react');//React.jsのライブラリをimport
const ReactDOM = require('react-dom');
//import React from '../../node_modules/react';
//コンポーネントを定義
class Index extends React.Component{
  render() {
    return (
        <p>hoge</p>
    );
  }
};

//id='content'の要素にコンポーネント「Index」を挿入してレンダリング
ReactDOM.render(<Index />, document.getElementById('content'));
